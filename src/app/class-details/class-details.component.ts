import {Component } from '@angular/core'
import { StorageService } from './../storage.service'
import { Student } from './../student'
@Component({

templateUrl: './class-details.component.html',
styleUrls:['class-details.component.css']

})
export class ClassDetailsComponent
{
   
    stu:Student
    student:Student[]
    public temp = 1
    
    


    constructor(private storageService:StorageService)
    {
            this.stu=new Student()
            this.student=storageService.readExistingData();
    }
  
    
    addStudent()
    {
        this.student.push(this.stu)
        this.storageService.writeInLocalArray(this.student)
       
    }

    updateStudent()
    {
     this.student[this.temp].name=this.stu.name
     this.student[this.temp].age=this.stu.age
     this.student[this.temp].class=this.stu.class
     this.storageService.writeInLocalArray(this.student)

    }
    
    editStudent(value,rowno){
        
        this.temp=rowno
        this.stu.name = value.name
        this.stu.class = value.email
        this.stu.age = value.phone
        
        this.temp = rowno
      
    }
   
    
    deleteStudent(stu1)
    {
       let index=this.student.indexOf(stu1)
        this.student.splice(index,1)
        this.storageService.writeInLocalArray(this.student)
    }
    

}