import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router'
import { appRoute} from './route'
import { LoginComponent } from './login/login.component'
import { ClassDetailsComponent } from './class-details/class-details.component'

import { LocalStorageModule } from 'angular-2-local-storage';
import { StorageService } from './storage.service'


import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ClassDetailsComponent
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
     RouterModule.forRoot(appRoute),
     LocalStorageModule.withConfig({
            prefix: 'my-app',
            storageType: 'localStorage'
        })
  ],
  providers: [StorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
