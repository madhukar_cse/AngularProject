import { Component } from '@angular/core'
import { Router } from '@angular/router'
@Component({
    selector:'login-comp',
    templateUrl: './login.component.html'

})
export class LoginComponent 
{

    constructor(private route:Router){

    }
    login(formValues)
    {
       if(formValues.userName==='admin' && formValues.passWord==='admin')
       {
           this.route.navigate(['/class'])
       }
       else{
           this.route.navigate(['/login'])
       }
    }
}