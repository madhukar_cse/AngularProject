import { Routes } from '@angular/router'

import { LoginComponent } from './login/login.component'
import { ClassDetailsComponent } from './class-details/class-details.component'

export const appRoute:Routes=[
    
    {path: 'login',component:LoginComponent},
    {path:'class', component:  ClassDetailsComponent},
    {path: '',redirectTo:'/login',pathMatch:'full'}
    

]